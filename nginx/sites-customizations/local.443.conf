server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;
    server_name NGINX_SERVER_NAME;
    NGINX_BASIC_AUTH_FOR_TEST_DOMAIN

    #ssl_certificate /etc/letsencrypt/live/NGINX_SERVER_NAME/fullchain.pem; # Activate this if you want secure ssl internally
    #ssl_certificate_key /etc/letsencrypt/live/NGINX_SERVER_NAME/privkey.pem; # Activate this if you want secure ssl internally
    ssl_certificate /etc/ssl/certs/ssl-cert-snakeoil.pem;
    ssl_certificate_key /etc/ssl/certs/ssl-cert-snakeoil.key;

    ssl_session_cache shared:SSL:20m;
    ssl_session_timeout 60m;

    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;

    add_header Strict-Transport-Security "max-age=31536000; includeSubDomains" always;

    location = /robots.txt {
      root /crawlers/;
    }

    location = /sitemap.xml {
        root /crawlers/;
    }

    location / {
        try_files $uri @prerender;
    }

    location NGINX_IMAGE_STATICFILES_DESTINATION_DIRECTORY/ {
        root /;
    }

    location NGINX_IMAGE_MEDIAFILES_DESTINATION_DIRECTORY/ {
        root /;
    }

    location /gitlab-badges/ {
        auth_basic off;
        root /;
    }

    location @prerender {
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $host;
        proxy_redirect off;
        client_max_body_size 50M; # allow up to 50MB user-provided file-upload size.

        set $prerender 0;
        if ($http_user_agent ~* "googlebot|bingbot|yandex|baiduspider|twitterbot|facebookexternalhit|rogerbot|linkedinbot|embedly|quora link preview|showyoubot|outbrain|pinterest\/0\.|pinterestbot|slackbot|vkShare|W3C_Validator|whatsapp") {
            set $prerender 1;
        }
        if ($args ~ "_escaped_fragment_") {
            set $prerender 1;
        }
        if ($http_user_agent ~ "Prerender") {
            set $prerender 0;
        }
        if ($uri ~* "\.(js|css|xml|less|png|jpg|jpeg|gif|pdf|doc|txt|ico|rss|zip|mp3|rar|exe|wmv|doc|avi|ppt|mpg|mpeg|tif|wav|mov|psd|ai|xls|mp4|m4a|swf|dat|dmg|iso|flv|m4v|torrent|ttf|woff|svg|eot)") {
            set $prerender 0;
        }

        #resolve using Google's DNS server to force DNS resolution and prevent caching of IPs
        resolver 8.8.8.8;

        if ($prerender = 1) {
            #setting prerender as a variable forces DNS resolution since nginx caches IPs and doesnt play well with load balancing
            #set $prerender "prerender:3000";
            rewrite .* /https://$host$request_uri? break;
            proxy_pass http://prerender_server;
        }
        if ($prerender = 0) {
            # everything is passed to Gunicorn
            proxy_pass http://app_server;
        }
    }

    gzip on;
    gzip_vary on;
    gzip_proxied expired no-cache no-store private auth;
    gzip_types text/plain text/css text/javascript text/xml image/gif image/jpeg image/png image/svg+xml image/x-icon application/javascript application/xml;
    gzip_min_length 10240;
    gzip_disable "MSIE [1-6]\.";
    gunzip on;
}
